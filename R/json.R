#' @method qread json
#' @export
qread.json <- function(file, type, ...) {
	.check_package("rjson");

	rjson::fromJSON(file = file)
}

#' @method qwrite json
#' @export
qwrite.json <- function(x, file, type, append=FALSE, ...) {
	.check_package("rjson");

	cat(rjson::toJSON(x), sep="", file=file, append=append)
}
